import React from 'react'
import PlacesAutocomplete from 'react-places-autocomplete'
import * as distance from 'google-distance-matrix'
import _ from 'lodash'
import PropTypes from 'prop-types'

class SimpleForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            origin: 'San Francisco, CA',
            destination: 'New York, NY'
        }
        this.onChangeOrigin = (origin) => this.setState({ origin: origin})
        this.onChangeDestination = (destination) => this.setState({ destination: destination})
        this.handleFormSubmit = this.handleFormSubmit.bind(this)
        this.showResults = this.showResults.bind(this)
    }
    static contextTypes = {
        router: PropTypes.object,
        location: PropTypes.object
    }

    componentDidMount () {
        if (this.context.location.query) {
            this.setState({
                origin: this.context.location.query.from,
                destination: this.context.location.query.to
            })
        }
    }

    handleFormSubmit = (event) => {
        event.preventDefault()

        var origins = [this.state.origin];
        var destinations = [this.state.destination];

        distance.key('AIzaSyDi-n-asH_OsWSsyhUOEIy6dcSQBkLgSLo');

        distance.matrix(origins, destinations, this.handleDistanceResponse.bind(this))
    }

    render() {
        const originProps = {
            value: this.state.origin,
            onChange: this.onChangeOrigin
        }

        const destinationProps = {
            value: this.state.destination,
            onChange: this.onChangeDestination
        }

        return (
            <div>
                <form onSubmit={this.handleFormSubmit}>
                    <PlacesAutocomplete inputProps={originProps} />
                    <PlacesAutocomplete inputProps={destinationProps} />
                    <button type="submit">Submit</button>
                </form>
                <div>
                    <span>{this.state.distance}</span>
                    <span>{this.state.duration}</span>
                    <span>{this.state.noResponse}</span>
                </div>
            </div>
        )
    };

    handleDistanceResponse(err, distances) {

        var noResponse = _.get(distances.rows[0].elements[0], 'status')

        if (!err && noResponse !== 'ZERO_RESULTS') {

            var distance = _.get(distances.rows[0].elements[0], 'distance.text');
            var duration = _.get(distances.rows[0].elements[0], 'duration.text');

            this.showResults()
            this.setState({
                distance: distance,
                duration: duration,
                noResponse: ''
            })
        } else {
            this.showResults()
            this.setState({
                distance: '',
                duration: '',
                noResponse: "No results found"
            })
        }
    };

    showResults () {
        this.context.router.push({
            pathname: '/results',
            query: {
                from: this.state.origin,
                to: this.state.destination
            },
        })
    }
}

export default SimpleForm